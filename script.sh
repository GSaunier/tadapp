curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=tadapp' \
  --data 'url=http://app:3000'

curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=hasura' \
  --data 'url=http://hasura:8080/v1/graphql' \
  --data 'path=/hasura'

curl -i -X POST \
  --url http://localhost:8001/services/hasura/routes \
  --data 'hosts[]=localhost' \
  --data 'paths[]=/hasura' \

curl -i -X POST \
  --url http://localhost:8001/services/tadapp/routes \
  --data 'hosts[]=localhost' \
  --data 'paths[]=/orders' \
  --data 'strip_path=false'

curl -i -X POST \
  --url http://localhost:8001/services/tadapp/routes \
  --data 'hosts[]=localhost' \
  --data 'strip_path=false'

curl -X POST \
  --url http://127.0.0.1:8001/services/hasura/plugins/ \
  --data "name=oauth2" \
  --data "config.scopes=access" \
  --data "config.mandatory_scope=true" \
  --data "config.enable_authorization_code=true"\
  --data "config.provision_key=provision_key"

curl -X POST \
  --url http://127.0.0.1:8001/services/tadapp/plugins/ \
  --data "name=oauth2" \
  --data "config.scopes=access" \
  --data "config.mandatory_scope=true" \
  --data "config.enable_authorization_code=true"\
  --data "config.provision_key=provision_key"

curl -X POST \
  --url "http://127.0.0.1:8001/consumers/" \
  --data "username=tada"

curl -X POST \
  --url "http://localhost:8001/consumers/tada/oauth2/" \
  --data "name=authapp" \
  --data "client_id=client_id"\
  --data "client_secret=client_secret" \
  --data "redirect_uris[]=http://192.168.99.100:8080/"

curl -X POST http://localhost:8001/services/hasura/plugins \
    --data "name=cors"  \
    --data "config.origins=*" \
    --data "config.methods=GET" \
    --data "config.methods=POST" \
    --data "config.methods=PUT" \
    --data "config.methods=DELETE" \
    --data "config.headers=Accept" \
    --data "config.headers=Authorization" \
    --data "config.headers=Host" \
    --data "config.headers=Accept-Version" \
    --data "config.headers=Content-Length" \
    --data "config.headers=Content-MD5" \
    --data "config.headers=Content-Type" \
    --data "config.headers=Date" \
    --data "config.headers=X-Auth-Token" \
    --data "config.exposed_headers=X-Auth-Token" \
    --data "config.credentials=true" \
    --data "config.max_age=3600"

curl -X POST http://localhost:8001/services/tadapp/plugins \
    --data "name=cors"  \
    --data "config.origins=*" \
    --data "config.methods=GET" \
    --data "config.methods=POST" \
    --data "config.methods=PUT" \
    --data "config.methods=DELETE" \
    --data "config.headers=Accept" \
    --data "config.headers=Authorization" \
    --data "config.headers=Host" \
    --data "config.headers=Accept-Version" \
    --data "config.headers=Content-Length" \
    --data "config.headers=Content-MD5" \
    --data "config.headers=Content-Type" \
    --data "config.headers=Date" \
    --data "config.headers=X-Auth-Token" \
    --data "config.exposed_headers=X-Auth-Token" \
    --data "config.credentials=true" \
    --data "config.max_age=3600"