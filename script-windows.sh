curl -i -X POST \
  --url http://192.168.99.100:8001/services/ \
  --data 'name=tadapp' \
  --data 'url=http://app:3000'

curl -i -X POST \
  --url http://192.168.99.100:8001/services/ \
  --data 'name=hasura' \
  --data 'url=http://hasura:8080/v1/graphql' \

curl -i -X POST \
  --url http://192.168.99.100:8001/services/hasura/routes \
  --data 'hosts[]=192.168.99.100' \
  --data 'paths[]=/hasura/http' \
  --data 'strip_path=true'

curl -i -X POST \
  --url http://192.168.99.100:8001/services/hasura/routes \
  --data 'hosts[]=192.168.99.100' \
  --data 'paths[]=/hasura/ws' \
  --data 'strip_path=true'

curl -i -X POST \
  --url http://192.168.99.100:8001/services/tadapp/routes \
  --data 'hosts[]=192.168.99.100' \
  --data 'paths[]=/orders' \
  --data 'strip_path=true'

curl -X POST \
  --url http://192.168.99.100:8001/services/hasura/plugins/ \
  --data "name=oauth2" \
  --data "config.scopes=access" \
  --data "config.mandatory_scope=true" \
  --data "config.enable_authorization_code=true"\
  --data "config.provision_key=provision_key"

curl -X POST \
  --url http://192.168.99.100:8001/services/tadapp/plugins/ \
  --data "name=oauth2" \
  --data "config.scopes=access" \
  --data "config.mandatory_scope=true" \
  --data "config.enable_authorization_code=true"\
  --data "config.provision_key=provision_key"

curl -X POST \
  --url "http://192.168.99.100:8001/consumers/" \
  --data "username=tada"

curl -X POST \
  --url "http://192.168.99.100:8001/consumers/tada/oauth2/" \
  --data "name=authapp" \
  --data "client_id=client_id"\
  --data "client_secret=client_secret" \
  --data "redirect_uris[]=http://192.168.99.100:8080/"

curl -X POST http://192.168.99.100:8001/services/hasura/plugins \
    --data "name=cors"  \
    --data "config.origins=*" \
    --data "config.methods=GET" \
    --data "config.methods=POST" \
    --data "config.methods=PUT" \
    --data "config.methods=DELETE" \
    --data "config.headers=Accept" \
    --data "config.headers=Authorization" \
    --data "config.headers=Host" \
    --data "config.headers=Accept-Version" \
    --data "config.headers=Content-Length" \
    --data "config.headers=Content-MD5" \
    --data "config.headers=Content-Type" \
    --data "config.headers=Date" \
    --data "config.headers=X-Auth-Token" \
    --data "config.exposed_headers=X-Auth-Token" \
    --data "config.credentials=true" \
    --data "config.max_age=3600"

curl -X POST http://192.168.99.100:8001/services/tadapp/plugins \
    --data "name=cors"  \
    --data "config.origins=*" \
    --data "config.methods=GET" \
    --data "config.methods=POST" \
    --data "config.methods=PUT" \
    --data "config.methods=DELETE" \
    --data "config.headers=Accept" \
    --data "config.headers=Authorization" \
    --data "config.headers=Host" \
    --data "config.headers=Accept-Version" \
    --data "config.headers=Content-Length" \
    --data "config.headers=Content-MD5" \
    --data "config.headers=Content-Type" \
    --data "config.headers=Date" \
    --data "config.headers=X-Auth-Token" \
    --data "config.exposed_headers=X-Auth-Token" \
    --data "config.credentials=true" \
    --data "config.max_age=3600"