#Add new Service

curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=tadapp' \
  --data 'url=http://app:3000'

#Add new Route for the service

curl -i -X POST \
  --url http://localhost:8001/services/tadapp/routes \
  --data 'hosts[]=localhost' \
  --data 'paths[]=/orders' \
  --data 'strip_path=false'

#Add oauth2 for the service

curl -X POST \
  --url http://127.0.0.1:8001/services/tadapp/plugins/ \
  --data "name=oauth2" \
  --data "config.scopes=access" \
  --data "config.mandatory_scope=true" \
  --data "config.enable_authorization_code=true"

#{
#  "created_at": 1585127438,
#  "config": {
#    "refresh_token_ttl": 1209600,
#    "enable_client_credentials": false,
#    "mandatory_scope": true,
#    "provision_key": "V2YVhsxFYbQaQNviL6CewUBa3Ff3yA5s",  //Keep the provision key
#    "accept_http_if_already_terminated": false,
#    "hide_credentials": false,
#    "enable_implicit_grant": false,
#    "global_credentials": false,
#    "enable_authorization_code": true,
#    "enable_password_grant": false,
#    "scopes": [
#      "access"
#    ],
#    "anonymous": null,
#    "token_expiration": 7200,
#    "auth_header_name": "authorization"
#  },
#  "id": "1d4f03c0-b2d0-451b-97cd-4f899c07a6a7",
#  "service": {
#    "id": "6be03504-2b7d-4b82-bd16-0e75cbad0f1e"
#  },
#  "enabled": true,
#  "protocols": [
#    "grpc",
#    "grpcs",
#    "http",
#    "https"
#  ],
#  "name": "oauth2",
#  "consumer": null,
#  "route": null,
#  "tags": null
#}

#Add new consumer

curl -X POST \
  --url "http://127.0.0.1:8001/consumers/" \
  --data "username=tada" \
| jq

#Create oauth2 app for the consumer

curl -X POST \
  --url "http://192.168.99.100:8001/consumers/tada/oauth2/" \
  --data "name=authapp" \
  --data "redirect_uris[]=http://192.168.99.100:8080/" \
| jq

#{
#  "redirect_uris": [
#    "http://app/redirect"  // Keep redirect uri
#  ],
#  "created_at": 1585127463,
#  "consumer": {
#    "id": "def8c0bf-a3d7-42bc-b43f-4561663a82e9"
#  },
#  "id": "4b7d2ec9-6d79-4196-b440-4c81feaa848f",
#  "tags": null,
#  "name": "authapp",
#  "client_secret": "Q4i4cTI194oLbKnCRS0l0pu97R891Bt5",  //Keep client_secret
#  "client_id": "fveldOB3Kz2ONID9Q3is6L9gMc1cA4Vl"  //Keep client_id
#}

#Fetch consumer credentials

curl -X GET \
  --url "http://127.0.0.1:8001/oauth2?client_id=pdy8fxGWmTIYGpXWVKYXJjM1QMiR6V6w" \
| jq

#{
#  "redirect_uris": [
#    "http://app/redirect"  // Keep redirect uri
#  ],
#  "created_at": 1585127463,
#  "consumer": {
#    "id": "def8c0bf-a3d7-42bc-b43f-4561663a82e9"
#  },
#  "id": "4b7d2ec9-6d79-4196-b440-4c81feaa848f",
#  "tags": null,
#  "name": "authapp",
#  "client_secret": "Q4i4cTI194oLbKnCRS0l0pu97R891Bt5",  //Keep client_secret
#  "client_id": "fveldOB3Kz2ONID9Q3is6L9gMc1cA4Vl"  //Keep client_id
#}

#Fetch authorization code with consumer credentials

curl --insecure -X POST \
  --url "https://localhost:8443/hasura/oauth2/authorize" \
  --data "client_id=client_id" \
  --data "response_type=code" \
  --data "provision_key=provision_key" \
  --data "authenticated_userid=tada" \
  --data "scope=access" \
| jq

#{
#  "redirect_uri": "http://app/redirect?code=w1c8Y0SM8HScsGvk96zN23rFHH8xtOMA" //Keep code
#}

#Fetch authorization token with code

curl --insecure -X POST \
  --url "https://localhost:8443/hasura/oauth2/token" \
  --data "client_id=client_id" \
  --data "client_secret=client_secret" \
  --data "code=g1Pt2pxIeXKhEf45KAuktiQAB1h16tN4" \
  --data "grant_type=authorization_code" \
  --data "redirect_uri=http://192.168.99.100:8080/" \
| jq

curl --insecure -X POST \
  --url "https://127.0.0.1:8443/hasura/oauth2/token" \
  --data "client_id=client_id" \
  --data "client_secret=client_secret" \
  --data "code=Nil29sZl9oUJWs3BW2CmNfa4xNLMzpIc" \
  --data "grant_type=authorization_code" \
  --data "redirect_uri=http://192.168.99.100:8080/" \
| jq


#{
#  "refresh_token": "KEDfNuOjrv8BMHsPO17RdY1L1lDKvf57",
#  "token_type": "bearer",
#  "access_token": "gHJNq3AD5osE2BQzyLnvD91RYj8tBzBZ", //Keep access_token
#  "expires_in": 7200
#}

#Use authorization token 

curl -X GET \
  --url "http://localhost:8000/haasura" \
  --header "Host: localhost" \
  --header "Authorization: bearer Kjk0n1BfDqVZifJbpPsG3wvwJTKLfL8R" \
| jq

curl -X GET \
  --url "http://192.168.99.100:8000/haasura" \
  --header "Authorization: bearer Kjk0n1BfDqVZifJbpPsG3wvwJTKLfL8R" \
| jq

curl -sX GET http://127.0.0.1:8001/oauth2_tokens/
