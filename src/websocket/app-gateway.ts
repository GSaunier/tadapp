import {OnGatewayConnection, WebSocketGateway, WebSocketServer} from "@nestjs/websockets";
import {Injectable, Logger} from "@nestjs/common";
import {Server, Socket} from "socket.io";


@WebSocketGateway({transport: ['websocket']})
@Injectable()
export class AppGateway implements OnGatewayConnection{

    @WebSocketServer()
    private server : Server;
    private logger: Logger = new Logger('AppGateway');

    emit(event : string, message : any) {
        Logger.log("Message envoyé","WebSocket");
        this.server.emit(event,message);
    }

    afterInit(server: Server) {
        this.logger.log('Init');
    }

    handleDisconnect(client: Socket) {
        this.logger.log(`Client disconnected: ${client.id}`);
    }

    handleConnection(client: Socket, ...args: any[]) {
        this.logger.log(`Client connected: ${client.id}`);
    }
}