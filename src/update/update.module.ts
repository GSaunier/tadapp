import {Module} from '@nestjs/common';
import {UpdateController} from './update.controller';
import {UpdateService} from './update.service';
import {MongooseModule} from '@nestjs/mongoose';
import {OrderSchema} from '../orders/schemas/order-schema';
import {AppGateway} from "../websocket/app-gateway";
import {OrderHasuraDao} from "../orders/dao/order-hasura-dao";

@Module({

  imports: [MongooseModule.forFeature([{ name: 'Order', schema: OrderSchema }])],
  controllers: [UpdateController],
  providers: [UpdateService,AppGateway,OrderHasuraDao]
})
export class UpdateModule {}
