import {Controller} from '@nestjs/common';
import {EventPattern} from '@nestjs/microservices';
import {Message} from '../orders/interfaces/message';
import {UpdateService} from './update.service';

@Controller('update')
export class UpdateController {

  constructor(private readonly _updateService: UpdateService) {
  }

  @EventPattern('update')
  updateOrder(data: Message) {
    this._updateService.update(data.id,data.update);
  }
}
