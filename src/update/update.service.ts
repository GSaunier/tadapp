import {Injectable} from '@nestjs/common';
import {UpdateOrderDto} from '../orders/dto/update-order-dto';
import {AppGateway} from "../websocket/app-gateway";
import {OrderHasuraDao} from "../orders/dao/order-hasura-dao";

@Injectable()
export class UpdateService {

  constructor(private readonly _ordersDAO: OrderHasuraDao, private readonly _appGateway : AppGateway) {

  }

  update(id: string, updateOrderDTO: UpdateOrderDto) : void {
    this._ordersDAO.update(id,updateOrderDTO);
  }


}
