import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {FastifyAdapter, NestFastifyApplication} from '@nestjs/platform-fastify';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {Logger, ValidationPipe} from '@nestjs/common';
import {OrdersModule} from './orders/orders.module';
import {RabbitMQServer} from "./rabbitmq/rabbitmq-server";
import * as Config from "config";


async function bootstrap() {

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true }),
      {
        cors: true
      }
  );

  app.connectMicroservice({
    strategy: new RabbitMQServer('update'),
  });

  const server = Config.get("server");

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );
  const options = new DocumentBuilder()
    .setTitle('Orders')
    .setDescription('Orders API')
    .setVersion('1.0')
    .addTag('Orders')
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    include: [OrdersModule]
  });
  SwaggerModule.setup('documentation', app, document);

  await app.startAllMicroservices();
  await app.listen(server.port, server.host);

  Logger.log(`Application served at http://localhost:${server.port}`, 'bootstrap');
  Logger.log(`Application documentation at http://localhost:${server.port}/documentation`, 'bootstrap');

}

bootstrap();
