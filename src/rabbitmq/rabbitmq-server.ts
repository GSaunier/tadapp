import * as amqp from 'amqplib';
import {CustomTransportStrategy, Server} from '@nestjs/microservices';
import * as Config from 'config';

export class RabbitMQServer extends Server implements CustomTransportStrategy {
    private server: amqp.Connection = null;
    private channel: amqp.Channel = null;
    private host: string;

    constructor(
        private readonly queue: string) {
        super();
        this.host = Config.get("rabbitmq.uri");
    }

    public async listen(callback: () => void) {
        await this.init();
        this.channel.consume(`${this.queue}`, this.handleMessage.bind(this), {
            noAck: false,
        });
    }

    public close() {
        this.channel && this.channel.close();
        this.server && this.server.close();
    }

    private async handleMessage(message) {
        const { content } = message;
        const messageObj = JSON.parse(content.toString());
        const pattern = JSON.stringify(messageObj.pattern);
        const handler = this.getHandlerByPattern(pattern);

        if(!handler)
            return ;

        this.channel.ack(message);

        await handler(messageObj.data);
    }


    private async init() {
        this.server = await amqp.connect(this.host);
        this.channel = await this.server.createChannel();
        this.channel.assertQueue(`${this.queue}`, { durable: false });
        this.channel.assertExchange('update','direct', { durable: true });
        this.channel.bindQueue('update','update','');
    }
}