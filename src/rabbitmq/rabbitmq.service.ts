import * as amqp from 'amqplib';
import {Injectable} from "@nestjs/common";
import * as Config from 'config';

@Injectable()
export class RabbitmqService {


    public async sendSingleMessage(messageObj) {
        const server = await amqp.connect(Config.get("rabbitmq.uri"));
        const channel = await server.createChannel();

        channel.publish('update','',Buffer.from(JSON.stringify(messageObj)));
    }
}
