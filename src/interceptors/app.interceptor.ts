import {CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor} from '@nestjs/common';
import {merge, Observable, of, throwError} from 'rxjs';
import {catchError, filter, flatMap, map, tap} from 'rxjs/operators';
import {ServerResponse} from 'http';
import {FastifyReply} from 'fastify';

@Injectable()
export class AppInterceptor implements NestInterceptor {
  /**
   * Class constructor
   */
  constructor() {
  }

  /**
   * Intercepts all HTTP requests and responses
   *
   * @param context
   * @param next
   */
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const response: FastifyReply<ServerResponse> = context.switchToHttp().getResponse<FastifyReply<ServerResponse>>();

      return next.handle()
        .pipe(
          catchError(e => { Logger.log(e); return  throwError(e) }),
          map(_ => of(_)),
          flatMap((obs: Observable<any>) =>
            merge(
              obs
                .pipe(
                  filter(_ => !!_),
                  map(_ => _),
                ),
              obs
                .pipe(
                  filter(_ => !_),
                  tap(_ => response.status(204)),
                  map(_ => _),
                ),
            )))
  }
}
