import {Exclude, Expose, Type} from 'class-transformer';
import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import {IsIn} from 'class-validator';

@Exclude()
export class OrderEntity {
  @ApiModelProperty({description: "ID in the DB", example: "5763cd4dc378a38ecd387737"})
  @Expose()
  @Type(() => String)
  id: string;

  @ApiModelProperty({description: 'Status of the order', example: ['Open', 'Processing', 'Completed']})
  @Expose()
  @Type(() => String)
  @IsIn(['Open','Processing','Completed'])
  status: string;

  @ApiModelProperty({ description: "Name of the order", example: 'Name'})
  @Expose()
  @Type(() => String)
  name: string;

  constructor(partial: Partial<OrderEntity>) {
    Object.assign(this, partial);
  }

}
