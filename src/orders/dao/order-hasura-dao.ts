import {InjectModel} from '@nestjs/mongoose';
import {Order} from '../interfaces/order';
import {from, Observable} from 'rxjs';
import {Injectable} from '@nestjs/common';
import {Model} from 'mongoose';
import {UpdateOrderDto} from '../dto/update-order-dto';
import {CreateOrderDto} from "../dto/create-order-dto";
import {HttpLink} from "apollo-link-http";
import {ApolloClient} from "apollo-client";
import {InMemoryCache} from "apollo-cache-inmemory";
import gql from "graphql-tag";
import fetcher from 'isomorphic-fetch';
import * as Config from "config";


@Injectable()
export class OrderHasuraDao {
  private link: HttpLink;

  constructor(@InjectModel('Order') private readonly _orderModel: Model<Order>) {
    this.link = new HttpLink({
      uri: Config.get("hasura.uri"),
      credentials: 'include',
      fetch: fetcher
    });
  }

  create(createOrderDto : CreateOrderDto): Observable<Order> {
    return from(this.getClient().mutate({
          mutation: gql`
          mutation($name: String, $status: String) {
            insert_Tadapp_orders(objects: {name: $name, status: $status}) {
              returning {
                name
                status
                id
              }
            }
          }
      `,
          variables: {
            name: createOrderDto.name,
            status: "Open"
          }
        }).then(
        res => res.data.insert_Tadapp_orders["returning"][0]
        )
    );

  }

  findAll(): Observable<Order[]> {
    return from(this.getClient().query({
      query: gql`
          query {
              Tadapp_orders {
                id
                name
                status
              }
          }
      `
    }).then(res => res.data.Tadapp_orders));
  }

  findOne(id:string): Observable<Order|undefined> {
    return from(this.getClient().query({
      query: gql`
          query MyQuery($id: Int) {
            Tadapp_orders(where: {id: {_eq: $id}}) {
              id
              name
              status
            }
          }
      `,
      variables: {id: id}
    }).then(res => res.data.Tadapp_orders[0]));

  }

  update(id: string, updateOrderDTO: UpdateOrderDto): Observable<Order> {
    return from(this.getClient().mutate({
      mutation: gql`
          mutation ($id: Int, $column : Tadapp_orders_set_input) {
            update_Tadapp_orders(where: {id: {_eq: $id}}, _set: $column) {
              returning {
                id
                name
                status
              }
            }
          }
      `,
      variables: {id: id, column:updateOrderDTO}
    }).then(res => res.data.update_Tadapp_orders["returning"][0]));
  }

  delete(id: string): Observable<Order> {
    return from(this.getClient().mutate({
          mutation: gql`
          mutation ($id: Int) {
            delete_Tadapp_orders(where: {id: {_eq: $id}}) {
              returning {
                id
                name
                status
              }
            }
          }
      `,
          variables: {
            id: id
          }
        }).then(
        res => res.data.delete_Tadapp_orders["returning"][0]
        )
    );
  }

  private getClient() : ApolloClient<any> {

    return new ApolloClient({
      link: this.link,
      cache: new InMemoryCache(),
    });
  }
}
