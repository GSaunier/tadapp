import * as mongoose from 'mongoose';

export const OrderSchema = new mongoose.Schema({
    status: {
      type: String,
      required: true,
      enum: ["Open","Processing", "Completed"]
    },
    name: {
        type: String,
        required: true
    }
  }, {
  toJSON: { virtuals: true },
  versionKey: false,
});
