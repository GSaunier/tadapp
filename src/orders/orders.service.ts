import {Injectable, NotFoundException,} from '@nestjs/common';
import {Observable, of, throwError} from 'rxjs';
import {catchError, flatMap, map} from 'rxjs/operators';
import {OrderEntity} from './entities/order-entity';
import {UpdateOrderDto} from './dto/update-order-dto';
import {RabbitmqService} from "../rabbitmq/rabbitmq.service";
import {CreateOrderDto} from "./dto/create-order-dto";
import {OrderHasuraDao} from "./dao/order-hasura-dao";

@Injectable()
export class OrdersService {
  constructor(private readonly _orderDao: OrderHasuraDao, private readonly _rabbitmqService : RabbitmqService) {
  }

  create(createOrderDto : CreateOrderDto): Observable<OrderEntity> {
    return this._orderDao.create(createOrderDto).pipe(
      map(order => new OrderEntity(order)),
    );
  }

  findAll(): Observable<OrderEntity[]> {
    return this._orderDao.findAll().pipe(
      map(orders => (!!orders && !!orders.length) ? orders.map(order => new OrderEntity(order)) : undefined),
    )
  }

  findOne(id: string): Observable<OrderEntity> {
    return this._orderDao.findOne(id).pipe(
      flatMap(order => !!order ?
        of(new OrderEntity(order))
        : throwError(new NotFoundException(`No order found with ID: ${id}`)))
    )
  }

  update(id: string, updateOrderDTO: UpdateOrderDto): Observable<string> {
    this._rabbitmqService.sendSingleMessage( {data: {id: id, update: updateOrderDTO},pattern: "update"});
    return of("Processing");
  }

  delete(id: string): Observable<void> {
    return this._orderDao.delete(id).pipe(
        catchError(e => throwError(new NotFoundException(e.message))),
        flatMap(order => !!order ?
          of(undefined) :
            throwError(new NotFoundException(`Order with id '${id}' not found`)),
        )
    );
  }
}
