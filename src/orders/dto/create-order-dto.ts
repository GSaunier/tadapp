import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import {IsNotEmpty} from 'class-validator';

export class CreateOrderDto {
  @ApiModelProperty({ description: "Name of the order", example: 'Name'})
  @IsNotEmpty()
  name: string;
}
