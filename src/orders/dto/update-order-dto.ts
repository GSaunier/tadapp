import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import {IsIn, IsOptional} from 'class-validator';

export class UpdateOrderDto {
  @ApiModelProperty({ description: 'Status', example: 'Open'})
  @IsIn(['Open','Processing','Completed'])
  @IsOptional()
  status: string;

  @ApiModelProperty({ description: "Name of the order", example: 'Name'})
  @IsOptional()
  name: string;
}
