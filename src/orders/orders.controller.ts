import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseInterceptors
} from '@nestjs/common';

import {OrdersService} from './orders.service';
import {Observable} from 'rxjs';
import {OrderEntity} from './entities/order-entity';
import {IdPH} from '../paramHandlers/idPH';
import {UpdateOrderDto} from './dto/update-order-dto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiImplicitParam,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiUnprocessableEntityResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import {CreateOrderDto} from "./dto/create-order-dto";

@ApiUseTags("Orders")
@Controller('orders')
@UseInterceptors(ClassSerializerInterceptor)
export class OrdersController {

  constructor(private readonly _ordersService: OrdersService) {}

  @ApiCreatedResponse({description: `return new order`, type: OrderEntity})
  @ApiBadRequestResponse({ description: 'Parameter and/or payload provided are not good', type: CreateOrderDto })
  @Post()
  create(@Body() createOrderDto : CreateOrderDto): Observable<OrderEntity> {
    return this._ordersService.create(createOrderDto);
  }

  @ApiOkResponse({description: 'List all orders', type: OrderEntity, isArray: true})
  @ApiNoContentResponse({description: 'No order found'})
  @Get()
  findAll(): Observable<OrderEntity[]> {
    return this._ordersService.findAll();
  }

  @ApiImplicitParam({name: 'id',description: 'ID of the order', type: String})
  @ApiOkResponse({description: `Get order with ID`, type: OrderEntity})
  @ApiNotFoundResponse({description: `No order for the ID`})
  @Get(':id')
  findOne(@Param() param : IdPH): Observable<OrderEntity> {
    return this._ordersService.findOne(param.id)
  }

  @ApiOkResponse({ description: 'Order updated', type: OrderEntity })
  @ApiNotFoundResponse({ description: 'No order for the ID' })
  @ApiBadRequestResponse({ description: 'Parameter and/or payload provided are not good' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiImplicitParam({name: 'id',description: 'ID of the order', type: String})
  @Put(':id')
  update(@Param() param : IdPH, @Body() updateOrderDTO: UpdateOrderDto): Observable<String> {
    return this._ordersService.update(param.id,updateOrderDTO);
  }

  @ApiOkResponse({ description: 'Order deleted', type: OrderEntity })
  @ApiNotFoundResponse({ description: 'No order for the ID' })
  @ApiImplicitParam({name: 'id',description: 'ID of the order', type: String})
  @Delete(':id')
  delete(@Param() param: IdPH): Observable<void> {
    return this._ordersService.delete(param.id);
  }
  
}
