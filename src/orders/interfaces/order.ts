import {Document} from 'mongoose';

export interface Order extends Document {
  id: string;
  status: string;
  name: string;
}
