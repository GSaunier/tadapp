import {UpdateOrderDto} from '../dto/update-order-dto';

export interface Message  {
  id: string;
  update: UpdateOrderDto;
}
