import {Module} from '@nestjs/common';
import {OrdersService} from './orders.service';
import {MongooseModule} from '@nestjs/mongoose';
import {OrderSchema} from './schemas/order-schema';
import {OrdersController} from './orders.controller';
import {RabbitmqService} from "../rabbitmq/rabbitmq.service";
import {OrderHasuraDao} from "./dao/order-hasura-dao";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Order', schema: OrderSchema }])
  ],
  controllers: [OrdersController],
  providers: [OrdersService, OrderHasuraDao,RabbitmqService]
})
export class OrdersModule {}
