import {IsNotEmpty, IsString} from 'class-validator';

export class IdPH {
  @IsString()
  @IsNotEmpty()
  id: string
}