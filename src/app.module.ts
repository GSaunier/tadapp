import {Module} from '@nestjs/common';
import {OrdersModule} from './orders/orders.module';
import {MongooseModule} from '@nestjs/mongoose';
import {UpdateModule} from './update/update.module';
import {RabbitmqModule} from './rabbitmq/rabbitmq.module';
import * as Config from "config";
import {AppGateway} from "./websocket/app-gateway";

@Module({
  imports: [
    OrdersModule,
    MongooseModule.forRoot(Config.get('mongodb.uri'), Config.get('mongodb.options')),
    UpdateModule,
    RabbitmqModule,
  ],
  controllers: [],
  providers: [AppGateway],
})
export class AppModule {}
